package com.coolingme;

/**
 * @author wangyue
 * @date 2022/9/25 15:41
 */
public interface ILoginService {

    /**
     * 登录
     *
     * @param username 用户名
     * @param password 密码
     * @return java.lang.String
     * @author wangyue
     * @date 2022/9/25 17:03
     */
    String login(String username, String password);

}
